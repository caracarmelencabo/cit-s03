package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class CalculatorServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 43283931101411089L;
	
	public void init() throws ServletException{
		System.out.println("*******************************************");
		System.out.println(" CalculatorServlet has been initialized");
		System.out.println("*******************************************");
	}
	
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException{ 
		PrintWriter out = res.getWriter();
		out.println("<h1>You are now using the calculator app</h1>");
		out.println("<p>To use the app, input two numbers and an operation.</p>");
		out.println("<p>Hit the submit button after filling in the details.</p>");
		out.println("<p>You will get the result shown in your browser!</p>");
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException{
		
		PrintWriter out = res.getWriter();
		try {
			//I decided to use double for the data types so that the results will be more accurate, especially for those results that have decimal places.
			double num1 = Double.parseDouble(req.getParameter("num1"));
			double num2 = Double.parseDouble(req.getParameter("num2"));
			String operation = req.getParameter(("operation"));
			double result;
			
			out.println("<p>The two numbers you provided are: " + num1 + ", " + num2 + "</p>");
			out.println("<p>The operation that you wanted is: " + operation + "</p>");
		
			if(operation.equals("add")) {
				result = num1+num2;
			}else if(operation.equals("subtract")) {
				result = num1-num2;
			}else if(operation.equals("multiply")) {
				result = num1*num2;
			}else if(operation.equals("divide")) {
				if(num2 == 0) {
					out.println("<p>The result is: Try again! Cannot divide by zero.</p>");
					return;
				}else {
					result = num1/num2;
				}
			}else {
				out.println("<p>The operation that you wanted does not exist! </p>");
				return;
			}
			
			out.println("<p>The result is: " + result + "</p>");
		}catch (NumberFormatException e) {
		    out.println("<p>Try again! Input a number only.</p>");
		}
	}
	
	public void destroy() {
		System.out.println("****************************************");
		System.out.println(" CalculatorServlet has been destroyed. ");
		System.out.println("****************************************");
	}

}
